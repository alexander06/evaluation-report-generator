# Evaluation Report Generator

This project corresponds to the bachelors' thesis "Creating a Pipeline for Reproducible Evaluation Report Generation" written at the Human-Centered Computing research group.

First Examiner: Prof. Dr. Claudia Müller-Birn

Second Examiner: Barry Linnert

You can find documentation in [the Wiki](https://git.imp.fu-berlin.de/alexander06/evaluation-report-generator/-/wikis/home)