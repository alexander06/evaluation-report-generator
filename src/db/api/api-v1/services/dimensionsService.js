import {
  getQuestionsWithDimension,
  getDimensionForQuestion,
} from "../../postgres.js";

const dimensions = [
  {
    A1: {
      en: "Course Design. Indicates how the lecturer designs their course.",
      de: "Lehrgestaltung. Drückt aus, wie die lehrende Person die Veranstaltung gestaltet",
    },
  },
  {
    A2: {
      en: "Integration of students. Indicates how well the lecturer interacts with students",
      de: "Einbinden von Studierenden. Drückt aus, wie die lehrende Person mit Studierenden interagiert",
    },
  },
  {
    B1: {
      en: "Adapting to students. Indicated how well the lecturer can change their course according to the needs of students",
      de: "Anpassung an Studierende. Drückt aus, wie die lehrende Person die eigene Veranstaltung auf die Bedürfnisse der Studierenden anpassen kann",
    },
  },
  {
    B2: {
      en: "Motivation of students. Indicates how well the lecturer can motivate students for their course",
      de: "Motivation der Studierenden. Drückt aus, wie gut die lehrende Person die Studierenden für die eigene Veranstaltung motivieren kann",
    },
  },
  {
    C1: {
      en: "Rules of Conduct. Indicates if the lecturer employs rules of conduct and how well they enforce them",
      de: "Verhaltensregeln. Drückt aus, ob die lehrende Person eigene Verhaltensregeln hat und wie gut diese durchgesetzt werden können",
    },
  },
  {
    C2: {
      en: "Time management. Indicates how well the lecturer uses the time they have",
      de: "Zeitmanagement. Drückt aus, wie gut die lehrende Person mit der verfügbaren Zeit umgehen kann",
    },
  },
];

const dimensionsService = {
  getDimension(id) {
    return dimensions.filter((dim) => Object.keys(dim)[0] == id);
  },
  getAllDimensions() {
    return dimensions;
  },
  async getQuestionsForDimension(id) {
    return getQuestionsWithDimension(id);
  },
  async getDimensionForQuestion(question) {
    let res = await getDimensionForQuestion(question);
    return { id: res.id };
  },
};

export default dimensionsService;
