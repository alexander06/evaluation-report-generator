import { getAnswers } from "../../postgres.js";

const answersService = {
  async getAnswers(id) {
    return await getAnswers(id);
  },
};

export default answersService;
