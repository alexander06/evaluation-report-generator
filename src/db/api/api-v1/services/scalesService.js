import { getAll, getScaleForQuestion } from "../../postgres.js";

const scalesService = {
  async getScales() {
    return await getAll("scale");
  },
  async getScaleForQuestion(question) {
    return await getScaleForQuestion(question);
  },
};

export default scalesService;
