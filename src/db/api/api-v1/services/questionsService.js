import { getAll, getQuestions } from "../../postgres.js";

const questionsService = {
  async getQuestions(id) {
    return await getQuestions(id);
  },
  async getAllQuestions() {
    return await getAll("question");
  },
};

export default questionsService;
