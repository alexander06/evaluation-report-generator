import { getAll } from "../../postgres.js";

const courseTypesService = {
  async getCourseTypes() {
    return await getAll("course-type");
  },
};

export default courseTypesService;
