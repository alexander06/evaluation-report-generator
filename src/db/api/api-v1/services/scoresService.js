import {
  createCourseIfNotExists,
  getAllScores,
  insertScores,
  getAllScoresForDimensionInInstitute,
  getAllScoresForDimension,
  getAllScoresForSemester,
  getAllScoresForInstitute,
  getAllScoresForInstituteFromSemester,
  getAllScoresForDimensionInSemester,
  deleteScoresForSemester,
  deleteScoreForQuestion,
} from "../../postgres.js";

const scoresService = {
  async getAllScores() {
    return await getAllScores();
  },
  async getAllScoresForSemester(semester) {
    return getAllScoresForSemester(semester);
  },
  async getAllScoresForInstitute(institute) {
    return await getAllScoresForInstitute(institute);
  },
  async getAllScoresForInstituteFromSemester(institute, semester) {
    return getAllScoresForInstituteFromSemester(institute, semester);
  },
  async getAllScoresForDimensionInSemester(semester, dimension) {
    return await getAllScoresForDimensionInSemester(semester, dimension);
  },
  async getAllScoresForDimension(dimension) {
    return await getAllScoresForDimension(dimension);
  },
  async getAllScoresForDimensionInInstitute(semester, dimension, institute) {
    return await getAllScoresForDimensionInInstitute(
      semester,
      dimension,
      institute,
    );
  },
  async postScores(scores, force = false) {
    // Transpose format
    let queries = [];

    for (let score of scores) {
      // Gets course ID
      let courseId = await createCourseIfNotExists(
        score.course_number,
        score.course_name,
        score.course_type,
        score.lecturer,
        score.semester,
        score.institute,
        score.answers,
        score.participants,
      );

      for (let subpoints of score.scores) {
        queries.push([courseId, subpoints.question, subpoints.score]);
      }
    }

    return await insertScores(queries, force);
  },
  async deleteScoresForSemester(semester) {
    return await deleteScoresForSemester(semester);
  },
  async deleteScoreForQuestion(courses, question) {
    return await deleteScoreForQuestion(courses, question);
  },
};

export default scoresService;
