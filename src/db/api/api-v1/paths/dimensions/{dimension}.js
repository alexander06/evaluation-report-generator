export default function (dimensionsService) {
  let operations = {
    GET,
  };

  async function GET(req, res, _next) {
    res
      .status(200)
      .json(await dimensionsService.getDimension(req.params.dimension));
  }

  GET.apiDoc = {
    summary: "Returns the dimensions belonging to the provided id",
    tags: ["questions"],
    operationId: "getDimension",
    parameters: [
      {
        in: "path",
        name: "dimension",
        required: true,
        type: "string",
        description: "The Id of the dimension",
      },
    ],
    responses: {
      200: {
        description: "The dimension",
        schema: {
          $ref: "#/definitions/Dimension",
        },
      },
      404: {
        description: "Not found",
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
