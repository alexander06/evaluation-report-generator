export default function (dimensionsService) {
  let operations = {
    GET,
  };

  async function GET(req, res, _next) {
    let answer = await dimensionsService.getQuestionsForDimension(
      req.params.dimension,
    );
    if (answer.length > 0) {
      res.status(200).json(answer);
    } else {
      res.status(404).end();
    }
  }

  GET.apiDoc = {
    summary: "Returns the questions belonging to the provided dimension",
    tags: ["questions"],
    operationId: "getQuestionsForDimension",
    parameters: [
      {
        in: "path",
        name: "dimension",
        required: true,
        type: "string",
        description: "The Id of the dimension",
      },
    ],
    responses: {
      200: {
        description: "Questions with the corresponding dimension",
        schema: {
          $ref: "#/definitions/Question",
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
