export default function () {
  let operations = {
    GET,
  };

  function GET(_req, res, _next) {
    res.status(200).send("Healthy");
  }

  GET.apiDoc = {
    summary: "Returns if the service runs",
    tags: ["telemetry"],
    operationId: "healthy",
    responses: {
      200: {
        description: "Returns the word 'healthy'",
      },
    },
  };

  return operations;
}
