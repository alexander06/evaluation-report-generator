export default function (scoresService) {
  let operations = {
    GET,
  };

  async function GET(req, res, _next) {
    res
      .status(200)
      .json(await scoresService.getAllScoresForDimension(req.params.dimension));
  }

  GET.apiDoc = {
    summary: "Returns scores for every course for the specified dimension",
    tags: ["scores"],
    operationId: "getAllScoresForDimension",
    parameters: [
      {
        in: "path",
        name: "dimension",
        required: true,
        description:
          "Dimension Name (accepts 'A', 'B', 'C' or subdimensions with 'A1', 'A2', 'B1', etc.)",
        type: "string",
      },
    ],
    responses: {
      200: {
        description: "A list of Scores",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/Score",
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
