import { course } from "../../../open-api-types.js";

export default function (scoresService) {
  let operations = {
    DELETE,
  };

  async function DELETE(req, res, _next) {
    res
      .status(200)
      .json(
        await scoresService.deleteScoreForQuestion(
          req.body,
          req.params.question,
        ),
      );
  }

  DELETE.apiDoc = {
    summary:
      "Removes the score for the provided Questions for the provided course",
    tags: ["scores"],
    operationId: "deleteScoreForQuestion",
    parameters: [
      {
        in: "body",
        name: "course",
        required: true,
        schema: {
          type: "array",
          items: {
            type: "object",
            properties: {
              ...course,
            },
          },
        },
      },
      {
        in: "path",
        name: "question",
        required: true,
        description: "Question UUID",
        type: "string",
      },
    ],
    responses: {
      200: {
        description: "Deleted Score",
        schema: {
          type: "object",
          properties: {
            question: {
              type: "string",
              description: "UUID of the scored question",
            },
            score: {
              type: "integer",
              description: "Calculated score for question",
            },
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };
  return operations;
}
