export default function (scoresService) {
  let operations = {
    GET,
    DELETE,
  };

  async function GET(req, res, _next) {
    res
      .status(200)
      .json(await scoresService.getAllScoresForSemester(req.params.semester));
  }

  async function DELETE(req, res, _next) {
    res
      .status(200)
      .json(await scoresService.deleteScoresForSemester(req.params.semester));
  }

  GET.apiDoc = {
    summary: "Returns scores for every course from the specified semester",
    tags: ["scores"],
    operationId: "getAllScoresForSemester",
    parameters: [
      {
        in: "path",
        name: "semester",
        required: true,
        description: "Semester Name",
        type: "string",
      },
    ],
    responses: {
      200: {
        description: "A list of Scores",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/Score",
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  DELETE.apiDoc = {
    summary: "Deletes scores for every course from the specified semester",
    tags: ["scores"],
    operationId: "deleteScoresForSemester",
    parameters: [
      {
        in: "path",
        name: "semester",
        required: true,
        description: "Semester Name",
        type: "string",
      },
    ],
    responses: {
      200: {
        description: "Returned after everything is deleted successfully",
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
