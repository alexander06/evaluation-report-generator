export default function (scoresService) {
  let operations = {
    GET,
  };

  async function GET(req, res, _next) {
    res
      .status(200)
      .json(
        await scoresService.getAllScoresForDimensionInSemester(
          req.params.semester,
          req.params.dimension,
        ),
      );
  }

  GET.apiDoc = {
    summary:
      "Returns scores for every course from the specified semester from the provided dimension",
    tags: ["scores"],
    operationId: "getAllScoresForDimensionInSemester",
    parameters: [
      {
        in: "path",
        name: "semester",
        required: true,
        description: "Semester Name",
        type: "string",
      },
      {
        in: "path",
        name: "dimension",
        required: true,
        description: "Dimension Name",
        type: "string",
      },
    ],
    responses: {
      200: {
        description: "A list of Scores",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/Score",
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };
  return operations;
}
