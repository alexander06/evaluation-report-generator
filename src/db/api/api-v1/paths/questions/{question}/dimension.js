export default function (dimensionsService) {
  let operations = {
    GET,
  };

  async function GET(req, res, _next) {
    res
      .status(200)
      .json(
        await dimensionsService.getDimensionForQuestion(req.params.question),
      );
  }

  GET.apiDoc = {
    summary: "Returns the dimension that belongs to the specified question",
    tags: ["questions"],
    operationId: "getDimensionForQuestion",
    parameters: [
      {
        in: "path",
        name: "question",
        required: true,
        type: "string",
        description: "The Id of the question",
      },
    ],
    responses: {
      200: {
        description: "A list of questions that match the requested id",
        schema: {
          type: "object",
          properties: {
            id: {
              type: "string",
              description: "Dimension Id",
            },
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
