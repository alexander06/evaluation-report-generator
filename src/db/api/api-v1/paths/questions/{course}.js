export default function (questionsService) {
  let operations = {
    GET,
  };

  async function GET(req, res, _next) {
    res
      .status(200)
      .json(await questionsService.getQuestions(req.params.course));
  }

  GET.apiDoc = {
    summary: "Returns all questions belonging to the provided course type id",
    tags: ["questions"],
    operationId: "getQuestions",
    parameters: [
      {
        in: "path",
        name: "course",
        required: true,
        type: "string",
        description: "The Id of the course",
      },
    ],
    responses: {
      200: {
        description: "A list of questions that match the requested id",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/Question",
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
