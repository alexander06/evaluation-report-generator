import { getCourse } from "../../postgres.js";
import { course, score } from "../open-api-types.js";

export default function (scoresService) {
  let operations = {
    GET,
    POST,
    PUT,
  };

  async function GET(_req, res, _next) {
    res.status(200).json(await scoresService.getAllScores());
  }

  async function POST(req, res, _next) {
    let success = await scoresService.postScores(req.body);
    // !success would always be true due to javascript object conversion
    if (success !== true) {
      const [course, question] = success;
      let course_info = (await getCourse(course))[0];
      return res.status(409).json({
        course_number: course_info.number,
        course_name: course_info.name,
        course_type: course_info.type,
        lecturer: course_info.lecturer,
        semester: course_info.semester,
        question: question,
      });
    }
    res.status(201).end();
  }

  async function PUT(req, res, _next) {
    res.status(201).json(await scoresService.postScores(req.body, true));
  }

  GET.apiDoc = {
    summary: "Returns scores for every course from every semester",
    tags: ["scores"],
    operationId: "getAllScores",
    responses: {
      200: {
        description: "A list of Scores",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/Score",
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  POST.apiDoc = {
    summary: "Inserts provided scores into the database",
    tags: ["scores"],
    operationId: "postScores",
    parameters: [
      {
        name: "scores",
        in: "body",
        schema: {
          type: "array",
          items: {
            type: "object",
            properties: {
              ...course,
              scores: {
                type: "array",
                items: {
                  type: "object",
                  properties: {
                    ...score,
                  },
                },
              },
            },
          },
        },
      },
    ],
    responses: {
      201: {
        description: "Gets returned if all scores were insterted successfully",
      },
      409: {
        description:
          "Gets returned if scores already exist. Returns duplicated scores",
        schema: {
          type: "object",
          properties: {
            ...course,
            question: {
              type: "string",
              description: "UUID of duplicated question",
            },
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  PUT.apiDoc = {
    summary: "Inserts provided scores into the database. Overrides duplicates",
    tags: ["scores"],
    operationId: "postScoresForce",
    parameters: [
      {
        name: "scores",
        in: "body",
        schema: {
          type: "array",
          items: {
            type: "object",
            properties: {
              ...course,
              scores: {
                type: "array",
                items: {
                  type: "object",
                  properties: {
                    ...score,
                  },
                },
              },
            },
          },
        },
      },
    ],
    responses: {
      201: {
        description: "Gets returned if all scores were insterted successfully",
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
