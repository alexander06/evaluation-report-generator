export default function (scalesService) {
  let operations = {
    GET,
  };

  async function GET(_req, res, _next) {
    res.status(200).json(await scalesService.getScales());
  }

  GET.apiDoc = {
    summary: "Returns all keys for the scales",
    tags: ["questions"],
    operationId: "getScales",
    responses: {
      200: {
        description: "A list of all scales",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/Scale",
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
