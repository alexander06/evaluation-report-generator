export default function (answersService) {
  let operations = {
    GET,
  };

  async function GET(req, res, _next) {
    res.status(200).json(await answersService.getAnswers(req.params.scale));
  }

  GET.apiDoc = {
    summary: "Returns all answers belonging to the provided scale key",
    tags: ["questions"],
    operationId: "getAnswers",
    parameters: [
      {
        in: "path",
        name: "scale",
        required: true,
        type: "string",
        description: "The key of the scale",
      },
    ],
    responses: {
      200: {
        description: "A list of answers that match the requested id",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/Answer",
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
