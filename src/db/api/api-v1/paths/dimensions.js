export default function (dimensionsService) {
  let operations = {
    GET,
  };

  async function GET(_req, res, _next) {
    res.status(200).json(await dimensionsService.getAllDimensions());
  }

  GET.apiDoc = {
    summary: "Returns all dimensions",
    tags: ["questions"],
    operationId: "getAllDimensions",
    responses: {
      200: {
        description: "A list of dimensions",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/Dimension",
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
