export default function (courseTypesService) {
  let operations = {
    GET,
  };

  async function GET(_req, res, _next) {
    res.status(200).json(await courseTypesService.getCourseTypes());
  }

  GET.apiDoc = {
    summary: "Returns all course Types",
    tags: ["questions"],
    operationId: "getCourseTypes",
    responses: {
      200: {
        description: "A list of course types",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/Course",
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
