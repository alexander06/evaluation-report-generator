export default function (scalesService) {
  let operations = {
    GET,
  };

  async function GET(req, res, _next) {
    let scale = await scalesService.getScaleForQuestion(req.params.question);
    if (scale == null) {
      res
        .status(404)
        .send(`${req.params.question} does not exist or doesn't have a scale`);
    } else {
      res.status(200).json(scale);
    }
  }

  GET.apiDoc = {
    summary: "Returns the answer scale belonging to the provided question id",
    tags: ["questions"],
    operationId: "getScaleForQuestion",
    parameters: [
      {
        in: "path",
        name: "question",
        required: true,
        type: "string",
        description: "The Id of the question",
      },
    ],
    responses: {
      200: {
        description: "The key belonging to the question",
        schema: {
          $ref: "#/definitions/Scale",
        },
      },
      default: {
        description: "An error occurred",
      },
      404: {
        description: "Not found",
      },
    },
  };

  return operations;
}
