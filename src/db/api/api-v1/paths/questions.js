export default function (questionsService) {
  let operations = {
    GET,
  };

  async function GET(_req, res, _next) {
    res.status(200).json(await questionsService.getAllQuestions());
  }

  GET.apiDoc = {
    summary: "Returns all questions",
    tags: ["questions"],
    operationId: "getAllQuestions",
    responses: {
      200: {
        description: "A list of questions",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/Question",
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
