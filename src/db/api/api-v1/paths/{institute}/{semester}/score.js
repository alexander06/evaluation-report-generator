export default function (scoresService) {
  let operations = {
    GET,
  };

  async function GET(req, res, _next) {
    res
      .status(200)
      .json(
        await scoresService.getAllScoresForInstituteFromSemester(
          req.params.institute,
          req.params.semester,
        ),
      );
  }

  GET.apiDoc = {
    summary:
      "Returns scores for every course from the specified semester and institute",
    tags: ["scores"],
    operationId: "getAllScoresForInstituteFromSemester",
    parameters: [
      {
        in: "path",
        name: "institute",
        required: true,
        description: "Institute Name",
        type: "string",
      },
      {
        in: "path",
        name: "semester",
        required: true,
        description: "Semester Name",
        type: "string",
      },
    ],
    responses: {
      200: {
        description: "A list of Scores",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/Score",
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
