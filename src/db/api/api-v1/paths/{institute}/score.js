export default function (scoresService) {
  let operations = {
    GET,
  };

  async function GET(req, res, _next) {
    res
      .status(200)
      .json(await scoresService.getAllScoresForInstitute(req.params.institute));
  }

  GET.apiDoc = {
    summary: "Returns scores for every course from the specified institute",
    tags: ["scores"],
    operationId: "getAllScoresForInstitute",
    parameters: [
      {
        in: "path",
        name: "institute",
        required: true,
        description: "Institute Name",
        type: "string",
      },
    ],
    responses: {
      200: {
        description: "A list of Scores",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/Score",
          },
        },
      },
      default: {
        description: "An error occurred",
      },
    },
  };

  return operations;
}
