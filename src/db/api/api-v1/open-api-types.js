//Provides Type Definitions for Open-API that should not show up as a model

export const course = {
  course_number: {
    type: "string",
    description: "Course Number according to the Campus Management",
  },
  course_name: {
    type: "string",
    description: "Name of the course",
  },
  course_type: {
    type: "string",
    description:
      "Type the course was evaluated as (either Lecture, Tutorial or Seminar)",
  },
  lecturer: {
    type: "string",
    description: "Name of the Person that was evaluated",
  },
  semester: {
    type: "string",
    description: "Semester the course was evaluated in",
    example: ["WiSe2020/21", "SoSe2020"],
  },
  institute: {
    type: "string",
    description: "Institute that the course is in",
    example: ["bio", "math", "cs"],
  },
  answers: {
    type: "integer",
    description: "Amount of answers to the question",
  },
  participants: {
    type: "integer",
    description: "Number of students that visited the course",
  },
};

export const score = {
  question: {
    type: "string",
    description: "UUID of the scored question",
  },
  score: {
    type: "integer",
    description: "Calculated score for question",
  },
};
