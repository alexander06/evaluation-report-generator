import { course, score } from "./open-api-types.js";

const apiDoc = {
  swagger: "2.0",
  basePath: "/v1",
  info: {
    title: "Evaluation Question API",
    version: "1.0.0",
  },
  definitions: {
    Answer: {
      type: "object",
      properties: {
        id: {
          type: "string",
          description: "UUID of the answer",
        },
        content: {
          type: "string",
          description: "Answer Choice",
        },
        value: {
          type: "integer",
          description: "Number value representing the weight of the answer",
        },
      },
      required: ["id", "content", "value"],
    },
    Course: {
      type: "object",
      properties: {
        id: {
          type: "string",
          description: "UUID for the type",
        },
        type: {
          type: "string",
          description: "Course Type",
        },
      },
      required: ["id", "type"],
    },
    Dimension: {
      type: "object",
      properties: {
        id: {
          type: "string",
          description:
            "Dimension shorthand, first a letter between A, B or C and then 1 or 2",
        },
        description: {
          type: "object",
          description: "Describes the dimension and what it displays",
          properties: {
            en: {
              type: "string",
              description: "English description",
            },
            de: {
              type: "string",
              description: "German description",
            },
          },
        },
      },
      required: ["id", "description"],
    },
    Question: {
      type: "object",
      properties: {
        id: {
          type: "string",
          description: "UUID of the question",
        },
        content: {
          type: "string",
          description: "Full question text",
        },
        scale: {
          type: "string",
          description: "References scale key",
        },
        dimension: {
          type: ["string", "null"],
          description: "References Dimension id",
        },
      },
      required: ["name"],
    },
    Scale: {
      type: "object",
      properties: {
        id: {
          type: "string",
          description: "Key of the scale",
        },
      },
    },
    Score: {
      type: "object",
      properties: {
        id: {
          type: "string",
          description: "Course UUID",
        },
        ...course,
        scores: {
          type: "array",
          items: {
            type: "object",
            properties: {
              ...score,
            },
          },
        },
      },
    },
  },
  paths: {},
};

export default apiDoc;
