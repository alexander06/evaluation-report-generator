import express from "express";
import { initialize } from "express-openapi";
import swaggerUi from "swagger-ui-express";

import v1Docs from "./api-v1/api-doc.js";
//Services
import v1QuestionsService from "./api-v1/services/questionsService.js";
import v1ScalesService from "./api-v1/services/scalesService.js";
import v1AnswersService from "./api-v1/services/answersService.js";
import v1CourseTypesService from "./api-v1/services/courseTypesService.js";
import v1DimensionsService from "./api-v1/services/dimensionsService.js";
import v1ScoresService from "./api-v1/services/scoresService.js";

//TODO: examples?

const app = express();
const port = 3000;

initialize({
  app,
  apiDoc: v1Docs,
  dependencies: {
    questionsService: v1QuestionsService,
    scalesService: v1ScalesService,
    answersService: v1AnswersService,
    courseTypesService: v1CourseTypesService,
    dimensionsService: v1DimensionsService,
    scoresService: v1ScoresService,
  },
  paths: "./api-v1/paths/",
  docsPath: "/api-docs",
});

app.use(express.json());

app.use(
  "/v1/docs",
  swaggerUi.serve,
  swaggerUi.setup(null, {
    swaggerOptions: {
      url: "/v1/api-docs",
    },
  }),
);

app.get("/", (_req, res) => {
  res.redirect("/v1/docs");
});

app.listen(port);
