import pg from "pg";
import format from "pg-format";
import "dotenv/config";

const client = new pg.Client({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  port: process.env.DB_PORT || 5432,
  database: process.env.DB_DATABASE,
});

await client.connect();

export async function getAll(table) {
  const res = await client.query(`SELECT * FROM ${pg.escapeIdentifier(table)}`);
  return res.rows;
}

export async function getQuestions(courseId) {
  const res = await client.query(
    'SELECT id, content, scale, dimension FROM question INNER JOIN (SELECT question FROM "question-belongs-to" WHERE course = $1) ON question.id = question',
    [courseId],
  );
  return res.rows;
}

export async function getAnswers(scale) {
  const res = await client.query(
    'SELECT id, content, value FROM answer INNER JOIN (SELECT answer FROM "answer-belongs-to" WHERE scale = $1) ON answer.id = answer',
    [scale],
  );
  return res.rows;
}

export async function getQuestionsWithDimension(dimensionId) {
  let res;
  if (["A", "B", "C"].includes(dimensionId)) {
    res = await client.query(
      "SELECT * FROM question WHERE dimension LIKE $1 || '%'",
      [dimensionId],
    );
  } else {
    res = await client.query("SELECT * FROM question WHERE dimension = $1", [
      dimensionId,
    ]);
  }
  return res.rows;
}

export async function getScaleForQuestion(questionId) {
  let res = await client.query("SELECT scale FROM question WHERE id = $1", [
    questionId,
  ]);
  if (res.rows.length > 0) {
    return { id: res.rows[0].scale };
  } else {
    return null;
  }
}

export async function getDimensionForQuestion(questionId) {
  let res = await client.query("SELECT dimension FROM question WHERE id = $1", [
    questionId,
  ]);

  if (res.rows.length > 0) {
    return { id: res.rows[0].dimension };
  } else {
    return null;
  }
}

function generateScoreStructure(rows) {
  let courses = [];

  for (let row of rows) {
    if (courses.filter((course) => course.id == row.id).length > 0) {
      courses
        .filter((course) => course.id == row.id)[0]
        .scores.push({
          question: row.question,
          score: row.score,
        });
    } else {
      courses.push({
        id: row.id,
        course_number: row.number,
        course_name: row.name,
        course_type: row.type,
        lecturer: row.lecturer,
        semester: row.semester,
        institute: row.institute,
        answers: row.answers,
        participants: row.participants,
        scores: [
          {
            question: row.question,
            score: row.score,
          },
        ],
      });
    }
  }

  return courses;
}

export async function getCourse(id) {
  console.log(id);
  let res = await client.query(
    "SELECT number, name, type, lecturer, semester FROM course WHERE id = $1",
    [id],
  );

  return res.rows;
}

export async function getAllScores() {
  let res = await client.query(
    "SELECT * FROM course INNER JOIN (SELECT * FROM score) on id = course",
  );
  if (res.rows.length == 0) {
    return [];
  }

  return generateScoreStructure(res.rows);
}

export async function createCourseIfNotExists(
  number,
  name,
  type,
  lecturer,
  semester,
  institute,
  answers,
  participants,
) {
  // Exists check
  let course = await client.query(
    "SELECT id FROM course where number = $1 AND name = $2 AND type = $3 AND lecturer = $4 and semester = $5 AND institute = $6",
    [number, name, type, lecturer, semester, institute],
  );

  if (course.rows.length == 0) {
    let res = await client.query(
      "INSERT INTO course(number, name, type, lecturer, semester, institute, answers, participants) VALUES($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id",
      [
        number,
        name,
        type,
        lecturer,
        semester,
        institute,
        answers,
        participants,
      ],
    );
    return res.rows[0].id;
  } else {
    return course.rows[0].id;
  }
}

export async function insertScores(scores, force = false) {
  if (!force) {
    try {
      await client.query(format("INSERT INTO score VALUES %L", scores));
      return true;
    } catch (e) {
      let lastPart = e.detail.split("=")[1];
      let tupleRegex = /\(.+,\s.+\)/g;
      let tuple = lastPart.match(tupleRegex)[0];
      return tuple.replace("(", "").replace(")", "").split(", ");
    }
  }

  await client.query(
    format(
      "INSERT INTO score VALUES %L ON CONFLICT (course, question) DO UPDATE SET course=EXCLUDED.course, question=EXCLUDED.question, score=EXCLUDED.score",
      scores,
    ),
  );
}

export async function getAllScoresForDimensionInInstitute(
  semester,
  dimension,
  institute,
) {
  let res = await client.query(
    "SELECT * FROM course INNER JOIN (SELECT * FROM score INNER JOIN (SELECT * FROM question) ON question=id) ON course.id = course WHERE semester=$1 AND institute=$2 AND dimension LIKE $3 || '%'",
    [semester, institute, dimension],
  );
  return generateScoreStructure(res.rows);
}

export async function getAllScoresForDimensionInSemester(semester, dimension) {
  let res = await client.query(
    "SELECT * FROM course INNER JOIN (SELECT * FROM score INNER JOIN (SELECT * FROM question) ON question=id) ON course.id = course WHERE semester=$1 AND dimension LIKE $2 || '%'",
    [semester, dimension],
  );
  return generateScoreStructure(res.rows);
}

export async function getAllScoresForDimension(dimension) {
  let res = await client.query(
    "SELECT * FROM course INNER JOIN (SELECT * FROM score INNER JOIN (SELECT id as questionId, dimension FROM question) ON question=questionId) ON course.id = course WHERE dimension LIKE $1 || '%'",
    [dimension],
  );
  return generateScoreStructure(res.rows);
}

export async function getAllScoresForSemester(semester) {
  let res = await client.query(
    "SELECT * FROM course INNER JOIN (SELECT * FROM score) on id = course WHERE semester = $1",
    [semester],
  );
  return generateScoreStructure(res.rows);
}

export async function getAllScoresForInstitute(institute) {
  let res = await client.query(
    "SELECT * FROM course INNER JOIN (SELECT * FROM score) on id = course WHERE institute = $1",
    [institute],
  );
  return generateScoreStructure(res.rows);
}

export async function getAllScoresForInstituteFromSemester(
  institute,
  semester,
) {
  let res = await client.query(
    "SELECT * FROM course INNER JOIN (SELECT * FROM score) on id = course WHERE institute = $1 AND semester = $2",
    [institute, semester],
  );
  return generateScoreStructure(res.rows);
}

export async function deleteScoresForSemester(semester) {
  await client.query("DELETE FROM course WHERE semester = $1", [semester]);
}

export async function deleteScoreForQuestion(courses, question) {
  for (let course of courses) {
    await client.query(
      "DELETE FROM score WHERE question = $1 AND course IN (SELECT id FROM course WHERE number = $2 AND name = $3 AND type = $4 AND lecturer = $5 AND semester = $6)",
      [
        question,
        course.course_number,
        course.course_name,
        course.course_type,
        course.lecturer,
        course.semester,
      ],
    );
  }
}

export async function kill() {
  await client.end();
}
