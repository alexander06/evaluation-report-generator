CREATE DATABASE evaluation;
-- Ensures Database connection
\connect evaluation

CREATE TABLE "answer" (
  "id" uuid NOT NULL PRIMARY KEY,
  "content" text NOT NULL,
  "value" integer NOT NULL
);

INSERT INTO "answer" ("id", "content", "value") VALUES
('ebd484ba-e7bf-11ee-9404-00620b2c9060', 'trifft gar nicht zu', -3),
('ebd5ac7e-e7bf-11ee-9404-00620b2c9060', 'trifft nicht zu', -2),
('ebd6ba7d-e7bf-11ee-9404-00620b2c9060', 'trifft eher nicht zu', -1),
('ebd7cd53-e7bf-11ee-9404-00620b2c9060', 'trifft teils, teils zu', 0),
('ebd91145-e7bf-11ee-9404-00620b2c9060', 'trifft eher zu', 1),
('ebda3a5c-e7bf-11ee-9404-00620b2c9060', 'trifft zu', 2),
('ebdb9745-e7bf-11ee-9404-00620b2c9060', 'trifft völlig zu', 3),
('ebdcd500-e7bf-11ee-9404-00620b2c9060', 'Ja', 1),
('ebde1a47-e7bf-11ee-9404-00620b2c9060', 'Nein', 0),
('ebdf7344-e7bf-11ee-9404-00620b2c9060', 'viel zu niedrig', -2),
('ebe0d460-e7bf-11ee-9404-00620b2c9060', 'eher zu niedrig', -1),
('ebe243e9-e7bf-11ee-9404-00620b2c9060', 'genau richtig', 2),
('ebe3b50b-e7bf-11ee-9404-00620b2c9060', 'eher zu hoch', -1),
('ebe4fa46-e7bf-11ee-9404-00620b2c9060', 'viel zu hoch', -2),
('ebe63af1-e7bf-11ee-9404-00620b2c9060', 'schwerer', -1),
('ebe7964b-e7bf-11ee-9404-00620b2c9060', 'kein Unterschied', 1),
('ebe8ea7c-e7bf-11ee-9404-00620b2c9060', 'leichter', 1),
('ebea3f96-e7bf-11ee-9404-00620b2c9060', 'kann ich nicht beurteilen', 0),
('ebeb9a3c-e7bf-11ee-9404-00620b2c9060', '0-2', 0),
('ebecf201-e7bf-11ee-9404-00620b2c9060', '>2-4', 0),
('ebee3a3e-e7bf-11ee-9404-00620b2c9060', '>4-6', 0),
('ebef5a7a-e7bf-11ee-9404-00620b2c9060', '>6-8', 0),
('ebf0b7d8-e7bf-11ee-9404-00620b2c9060', '>8-10', 0),
('ebf1fe80-e7bf-11ee-9404-00620b2c9060', '>10-12', 0),
('ebf30aa3-e7bf-11ee-9404-00620b2c9060', '>12', 0);

CREATE TABLE scale (
  "id" varchar(256) NOT NULL PRIMARY KEY
);

INSERT INTO scale ("id") VALUES
('scale_applies'),
('scale_difficulty'),
('scale_height'),
('scale_time'),
('y_n');

CREATE TABLE "answer-belongs-to" (
  "answer" uuid NOT NULL,
  "scale" varchar(256) NOT NULL,
  PRIMARY KEY("answer", "scale"),
  CONSTRAINT fk_answer
    FOREIGN KEY("answer")
      REFERENCES "answer"("id"),
  CONSTRAINT fk_scale
    FOREIGN KEY("scale")
      REFERENCES "scale"("id")
);

INSERT INTO "answer-belongs-to" ("answer", "scale") VALUES
('ebd484ba-e7bf-11ee-9404-00620b2c9060', 'scale_applies'),
('ebd5ac7e-e7bf-11ee-9404-00620b2c9060', 'scale_applies'),
('ebd6ba7d-e7bf-11ee-9404-00620b2c9060', 'scale_applies'),
('ebd7cd53-e7bf-11ee-9404-00620b2c9060', 'scale_applies'),
('ebd91145-e7bf-11ee-9404-00620b2c9060', 'scale_applies'),
('ebda3a5c-e7bf-11ee-9404-00620b2c9060', 'scale_applies'),
('ebdb9745-e7bf-11ee-9404-00620b2c9060', 'scale_applies'),
('ebdcd500-e7bf-11ee-9404-00620b2c9060', 'y_n'),
('ebde1a47-e7bf-11ee-9404-00620b2c9060', 'y_n'),
('ebdf7344-e7bf-11ee-9404-00620b2c9060', 'scale_height'),
('ebe0d460-e7bf-11ee-9404-00620b2c9060', 'scale_height'),
('ebe243e9-e7bf-11ee-9404-00620b2c9060', 'scale_height'),
('ebe3b50b-e7bf-11ee-9404-00620b2c9060', 'scale_height'),
('ebe4fa46-e7bf-11ee-9404-00620b2c9060', 'scale_height'),
('ebe63af1-e7bf-11ee-9404-00620b2c9060', 'scale_difficulty'),
('ebe7964b-e7bf-11ee-9404-00620b2c9060', 'scale_difficulty'),
('ebe8ea7c-e7bf-11ee-9404-00620b2c9060', 'scale_difficulty'),
('ebea3f96-e7bf-11ee-9404-00620b2c9060', 'scale_difficulty'),
('ebeb9a3c-e7bf-11ee-9404-00620b2c9060', 'scale_time'),
('ebecf201-e7bf-11ee-9404-00620b2c9060', 'scale_time'),
('ebee3a3e-e7bf-11ee-9404-00620b2c9060', 'scale_time'),
('ebef5a7a-e7bf-11ee-9404-00620b2c9060', 'scale_time'),
('ebf0b7d8-e7bf-11ee-9404-00620b2c9060', 'scale_time'),
('ebf1fe80-e7bf-11ee-9404-00620b2c9060', 'scale_time'),
('ebf30aa3-e7bf-11ee-9404-00620b2c9060', 'scale_time');

CREATE TABLE "course-type" (
  "id" uuid NOT NULL PRIMARY KEY,
  "type" text NOT NULL
);

INSERT INTO "course-type" ("id", "type") VALUES
('a131b6b0-e7c4-11ee-9404-00620b2c9060', 'lecture'),
('a131d453-e7c4-11ee-9404-00620b2c9060', 'tutorial'),
('a131f499-e7c4-11ee-9404-00620b2c9060', 'seminar'),
('a1320e71-e7c4-11ee-9404-00620b2c9060', 'leko');

CREATE TABLE "question" (
  "id" uuid NOT NULL PRIMARY KEY,
  "content" text NOT NULL,
  "scale" varchar(256) NOT NULL,
  "dimension" varchar(2),
  CONSTRAINT fk_scale
    FOREIGN KEY("scale")
      REFERENCES "scale"("id")
);

INSERT INTO "question" ("id", "content", "scale", "dimension") VALUES
('fb95e4c0-e7c6-11ee-9404-00620b2c9060', 'Ich verfügte bereits vor dem Lehrveranstaltungsbesuch über umfangreiches Wissen zu den in der LV behandelten Themengebieten', 'scale_applies', NULL),
('fb9665de-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende macht den Stellenwert der Lerninhalte für das weitere Studium klar', 'scale_applies', 'B2'),
('fb969730-e7c6-11ee-9404-00620b2c9060', 'Ich hatte jederzeit einen Überblick über anstehende Termine und zu erledigende Aufgaben', 'scale_applies', 'A1'),
('fb96b299-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende gestaltet Tafelbild, Folien oder Powerpoint-Präsentationen leserlich und übersichtlich', 'scale_applies', 'A1'),
('fb96d0fa-e7c6-11ee-9404-00620b2c9060', 'Das Thema der Lehrveranstaltung hat mich schon vorher interessiert', 'scale_applies', NULL),
('fb96eba6-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende eröffnet den Studierenden Möglichkeiten, sich mit interessanten Inhalten eingehender zu beschäftigen', 'scale_applies', 'B2'),
('fb97039b-e7c6-11ee-9404-00620b2c9060', 'Ich habe die im Rahmen der Lehrveranstaltung gestellten Aufgaben umfassend bearbeitet', 'scale_applies', 'B2'),
('fb971d70-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende regt die Studierenden an, sich mit den Lerninhalten auch außerhalb der Veranstaltung auseinanderzusetzen', 'scale_applies', 'B2'),
('fb9736f1-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende fasst regelmäßig die wichtigsten Inhalte der LV zusammen', 'scale_applies', 'A1'),
('fb9751c6-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende gibt ein konstruktives Feedback auf die Beiträge/Antworten der Studierenden', 'scale_applies', 'A2'),
('fb9769f3-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende gestaltet seine/ihre Lehrveranstaltung abwechslungsreich', 'scale_applies', 'B2'),
('fb9783d2-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende erreicht, dass es mir Spaß macht, an der Lehrveranstaltung teilzunehmen bzw. die Inhalte der Lerneinheiten zu bearbeiten', 'scale_applies', 'B2'),
('fb97a4cf-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende gibt mir konkrete Hinweise zur Verbesserung meiner Leistungen', 'scale_applies', 'A2'),
('fb97c410-e7c6-11ee-9404-00620b2c9060', 'Wenn ja, sind die Lernmaterialien hilfreich', 'scale_applies', 'A1'),
('fb97e39a-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende erreicht, dass ich der Lehrveranstaltung aufmerksam folge', 'scale_applies', 'B2'),
('fb98027a-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende sorgt für eine angenehme Lernatmosphäre', 'scale_applies', 'B1'),
('fb98204a-e7c6-11ee-9404-00620b2c9060', 'Der Umgang mit rein digitalen Kursinhalten im Vergleich zu regelmäßigen wöchentlichen Präsenzveranstaltungen ist', 'scale_difficulty', NULL),
('fb984109-e7c6-11ee-9404-00620b2c9060', 'Ich war an mindestens zwei Drittel der Termine dieser Lehrveranstaltung anwesend', 'y_n', NULL),
('fb985e1a-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende trägt anregend und engagiert vor', 'scale_applies', 'B2'),
('fb987b71-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende stellt Fragen, die den Studierenden die Gelegenheit geben zu überprüfen, ob sie den Inhalt verstanden haben', 'scale_applies', 'A2'),
('fb989861-e7c6-11ee-9404-00620b2c9060', 'Ich verfügte bereits vor dem Veranstaltungsbesuch über umfangreiches Wissen zu den in der Lehrveranstaltung behandelten Themengebieten', 'scale_applies', NULL),
('fb98b420-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende stellt zu Beginn einer Sitzung den Zusammenhang zur letzen Sitzung her', 'scale_applies', 'A1'),
('fb98d60e-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende gibt mir ein konstruktives Feedback zu meinen Beiträgen/Lösungen', 'scale_applies', 'A2'),
('fb98f563-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende stellt immer wieder Bezüge zu bereits behandelten Lerninhalten her', 'scale_applies', 'A1'),
('fb9915da-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende fesselt die Studierenden durch eine anregende und engagierte Vortragsweise', 'scale_applies', 'B2'),
('fb993295-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende setzt Modelle, Graphiken oder Schemata so ein, dass sie das Verständnis komplexer Sachverhalte erleichtern', 'scale_applies', 'A1'),
('fb994be2-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende gibt mir zu wenige Rückmeldungen zu meinen Beiträgen/Lösungen', 'scale_applies', 'A2'),
('fb996bf0-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende nimmt die Fragen/Anregungen der Studierenden ernst', 'scale_applies', 'B1'),
('fb998cc4-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende bringt wichtige Inhalte gut auf den Punkt', 'scale_applies', 'C2'),
('fb99ad31-e7c6-11ee-9404-00620b2c9060', 'Insgesamt bin ich mit dieser Lehrveranstaltung zufrieden', 'scale_applies', NULL),
('fb99cb87-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende unterstützt Studierende bei Lernschwierigkeiten', 'scale_applies', 'B1'),
('fb99e833-e7c6-11ee-9404-00620b2c9060', 'Wieviele Stunden pro Woche brauchen Sie ungefähr für die Vor- und Nachbereitung der Lehrveranstaltung (inklusive Hausaufgaben)', 'scale_time', NULL),
('fb9a0a66-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende stellt immer wieder Bezüge zu den bereits behandelten Lerninhalten her', 'scale_applies', 'A1'),
('fb9a2c05-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende hat die gesamte Lehrveranstaltung gut strukturiert und nachvollziehbar gegliedert', 'scale_applies', 'A1'),
('fb9a4ce2-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende nimmt die Beiträge der Studierenden ernst', 'scale_applies', 'B1'),
('fb9a6674-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende erreicht, dass die Studierenden der Lehrveranstaltung aufmerksam folgen', 'scale_applies', 'B2'),
('fb9a7a96-e7c6-11ee-9404-00620b2c9060', 'Der Stoffumfang, der in der Veranstaltung behandelt wird, ist für mich', 'scale_height', 'A1'),
('fb9a9cc6-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende regt die Studierenden an, sich mit den Lehrinhalten auch über die Lehrveranstaltung hinaus auseinanderzusetzen', 'scale_applies', 'B2'),
('fb9ac005-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende gibt anschauliche Beispiele, die zum Verständnis des Lerninhaltes/Stoffs beitragen', 'scale_applies', 'A1'),
('fb9ae34b-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende passt das Anforderungsniveau der Lehrveranstaltung den Voraussetzungen der Studierenden gut an', 'scale_applies', 'B1'),
('fb9afb0b-e7c6-11ee-9404-00620b2c9060', 'Das Tempo der Veranstaltung ist für mich', 'scale_height', 'A1'),
('fb9b1f22-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende verdeutlicht die Lernziele zu Beginn jedes Termins/jeder Lerneinheit', 'scale_applies', 'A1'),
('fb9b40e8-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende stellt zu Beginn eines Termins/einer Lerneinheit den Zusammenhang zu dem Termin/der Lerneinheit davor her', 'scale_applies', 'A1'),
('fb9b648e-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende gibt hilfreiche und zielführende Antworten auf die Nachfragen der Studierenden', 'scale_applies', 'A2'),
('fb9b879c-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende vergewissert sich, dass die Studierenden zentrale Aspekte verstanden haben, bevor er/sie im Stoff weitergeht', 'scale_applies', 'A2'),
('fb9baf22-e7c6-11ee-9404-00620b2c9060', 'Der Stoffumfang der in der LV behandelt wird, ist für mich', 'scale_height', 'A1'),
('fb9bd3e1-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende ist in der Lage, die Studierenden für die in der Lehrveranstaltung behandelten Inhalte zu interessieren', 'scale_applies', 'B2'),
('fb9bf803-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende gibt den Studierenden zu wenige Rückmeldungen zu ihren Beiträgen/Antworten', 'scale_applies', 'A2'),
('fb9c1da7-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende ist bei Fragen gut erreichbar', 'scale_applies', 'A2'),
('fb9c4149-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende nutzt die verfügbare Zeit effektiv', 'scale_applies', 'C2'),
('fb9c6523-e7c6-11ee-9404-00620b2c9060', 'Ich habe an mindestens zwei Drittel der Termine dieser Lehrveranstaltung teilgenommen bzw. die Inhalte der Lerneinheiten bearbeitet', 'y_n', NULL),
('fb9c8f5a-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende verdeutlicht den Anwendungsbezug der Lerninhalte', 'scale_applies', 'B2'),
('fb9cb6cb-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende regt die Studierenden dazu an, die Richtigkeit ihrer Beitrage/Antworten selbst zu überprüfen', 'scale_applies', 'B2'),
('fb9cdee4-e7c6-11ee-9404-00620b2c9060', 'Der Schwierigkeitsgrad der Veranstaltung ist für mich', 'scale_height', 'A1'),
('fb9d084c-e7c6-11ee-9404-00620b2c9060', 'Werden im Rahmen der Lehrveranstaltung Lernmaterialien (z.B. Videoaufzeichnungen) online zur Verfügung gestellt', 'y_n', NULL),
('fb9d32f3-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende erklärt neue Begriffe und Konzepte klar und nachvollziehbar', 'scale_applies', 'A1'),
('fb9d5c64-e7c6-11ee-9404-00620b2c9060', 'Ich verfügte bereits vor der Lehrveranstaltung über viel Erfahrung mit E-Learning', 'scale_applies', NULL),
('fb9d832d-e7c6-11ee-9404-00620b2c9060', 'In dieser Lehrveranstaltung habe ich viel dazugelernt', 'scale_applies', NULL),
('fb9daf32-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende ist in der Lage die Studierenden für die in der Lehrveranstaltung behandelten Inhalte zu interessieren', 'scale_applies', 'B2'),
('fb9ddb73-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende steuert die Diskussion in der Lerngruppe zielführend', 'scale_applies', 'C1'),
('fb9e001c-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende geht auf die Interessen der Studierenden ein', 'scale_applies', 'B1'),
('fb9e281c-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende regt die Studierenden an, sich mit den Lehrinhalten auch außerhalb der Veranstaltung auseinanderzusetzen', 'scale_applies', 'B2'),
('fb9e5842-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende präsentiert die Lerninhalte/Lernmaterialien stimmig und kohärent', 'scale_applies', 'A1'),
('fb9e814b-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende ist in der Lage, auch einen komplexen Sachverhalt verständlich zu erklären', 'scale_applies', 'A1'),
('fb9eabdb-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende fasst regelmäßig die wichtigsten Inhalte der Lehrveranstaltung zusammen', 'scale_applies', 'A1'),
('fb9ecfe2-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende ist in der Lage, mich für die in der Lehrveranstaltung behandelten Inhalte zu interessieren', 'scale_applies', 'B2'),
('fb9ef893-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende achtet darauf, dass wir die aufgestellten Verhaltensregeln einhalten', 'scale_applies', 'C1'),
('fb9f2334-e7c6-11ee-9404-00620b2c9060', 'Das Thema der LV hat mich schon vorher interessiert', 'scale_applies', NULL),
('fb9f4cb4-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende verdeutlicht den Anwendungsbezug der Lerninhalte/des Stoffs', 'scale_applies', 'B2'),
('fb9f75cf-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende passt das Anforderungsniveau der Lehrveranstaltung dem Voraussetzungen der Studierenden gut an', 'scale_applies', 'B1'),
('fb9fa4e0-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende setzt zielführend audiovisuelle Medien (z.B. Powerpoint-Präsentationen, Ton- oder Bildmaterial) zur Vermittlung von Sachverhalten ein', 'scale_applies', 'A1'),
('fb9fd05d-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende hat klare Verhaltensregeln für unsere Zusammenarbeit in dieser Lehrveranstaltung kommuniziert', 'scale_applies', 'C1'),
('fb9ffca2-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende ermutigt die Studierenden bei der Aneignung schwieriger Inhalte', 'scale_applies', 'B1'),
('fba02cac-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende hat ein sehr gutes Zeitmanagement', 'scale_applies', 'C2'),
('fba05299-e7c6-11ee-9404-00620b2c9060', 'Der/die Lehrende gestaltet Tafelbild/Whiteboard, Folien oder Powerpoint-Präsentationen leserlich und übersichtlich', 'scale_applies', 'A1');

CREATE TABLE "question-belongs-to" (
  "question" uuid NOT NULL,
  "course" uuid NOT NULL,
  PRIMARY KEY("question", "course"),
  CONSTRAINT fk_question
    FOREIGN KEY("question")
      REFERENCES "question"("id"),
  CONSTRAINT fk_course
    FOREIGN KEY("course")
      REFERENCES "course-type"("id")
);

INSERT INTO "question-belongs-to" ("question", "course") VALUES
('fb95e4c0-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9665de-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9665de-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9665de-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9665de-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb969730-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb969730-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb969730-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb96b299-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb96b299-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb96d0fa-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb96eba6-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb96eba6-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb97039b-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb97039b-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb97039b-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb971d70-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb971d70-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9736f1-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9751c6-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9769f3-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9769f3-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9769f3-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9783d2-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb97a4cf-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb97c410-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb97c410-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb97c410-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb97e39a-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb98027a-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb98027a-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb98204a-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb98204a-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb98204a-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb984109-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb984109-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb984109-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb985e1a-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb987b71-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb989861-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb98b420-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb98d60e-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb98f563-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9915da-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9915da-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9915da-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb993295-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb993295-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb993295-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb994be2-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb996bf0-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb998cc4-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb99ad31-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb99ad31-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb99ad31-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb99ad31-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb99cb87-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb99e833-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb99e833-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9a0a66-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9a0a66-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9a0a66-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9a2c05-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9a2c05-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9a2c05-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9a2c05-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9a4ce2-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9a6674-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9a6674-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9a7a96-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9a7a96-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9a9cc6-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9ac005-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9ae34b-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9ae34b-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9ae34b-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9afb0b-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9afb0b-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9b1f22-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9b40e8-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9b648e-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9b648e-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9b648e-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9b648e-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9b879c-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9b879c-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9b879c-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9b879c-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9baf22-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9bd3e1-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9bd3e1-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9bf803-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9c1da7-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9c4149-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9c6523-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9c8f5a-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9cb6cb-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9cdee4-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9cdee4-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9cdee4-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9d084c-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9d084c-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9d084c-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9d32f3-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9d32f3-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9d32f3-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9d5c64-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9d832d-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9d832d-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9d832d-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9d832d-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9daf32-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9ddb73-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9e001c-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9e001c-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9e281c-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9e5842-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9e814b-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9eabdb-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9ecfe2-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9ef893-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9f2334-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9f4cb4-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fb9f4cb4-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fb9f4cb4-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9f75cf-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fb9fa4e0-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9fd05d-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060'),
('fb9ffca2-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fba02cac-e7c6-11ee-9404-00620b2c9060', 'a131b6b0-e7c4-11ee-9404-00620b2c9060'),
('fba02cac-e7c6-11ee-9404-00620b2c9060', 'a131d453-e7c4-11ee-9404-00620b2c9060'),
('fba02cac-e7c6-11ee-9404-00620b2c9060', 'a131f499-e7c4-11ee-9404-00620b2c9060'),
('fba05299-e7c6-11ee-9404-00620b2c9060', 'a1320e71-e7c4-11ee-9404-00620b2c9060');

-- Evaluation results
CREATE TABLE "course" (
  "id" uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  "number" varchar(256) NOT NULL,
  "name" varchar(256) NOT NULL,
  "type" varchar(256) NOT NULL,
  "lecturer" varchar(256) NOT NULL,
  "semester" varchar(256) NOT NULL,
  "institute" varchar(4) NOT NULL,
  "answers" integer NOT NULL,
  "participants" integer NOT NULL,
  -- Technically this could be a PK, but id makes access easier
  UNIQUE("number", "name", "type", "lecturer", "semester")
);

-- Cascade ensures that scores are deleted with courses
CREATE TABLE "score" (
  "course" uuid NOT NULL,
  "question" uuid NOT NULL,
  "score" integer NOT NULL,
  PRIMARY KEY("course", "question"),
  CONSTRAINT fk_course
    FOREIGN KEY("course")
      REFERENCES "course"("id")
        ON DELETE cascade,
  CONSTRAINT fk_question
    FOREIGN KEY("question")
      REFERENCES "question"("id")
        ON DELETE cascade
);