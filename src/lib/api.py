import requests

BASE_URL = "http://localhost:50000"


def request_questions() -> list[dict]:
    """Requests all questions that exist in the question API.
    Calls the `/questions` Endpoint

    Returns
    -------
    list[dict]: Array of questions, where the dict represents the question.
        Contains `id` (uuid), `content` (string), `scale` (string), `dimension` (string or null)
    """
    res = requests.get("{0}/v1/questions".format(BASE_URL))
    if res.status_code != 200:
        raise requests.RequestException(
            f"Request was not fullfilled successfully. Server responded with {res.status_code}")
    return res.json()


def get_questions_for_dimension(dimension: str) -> list[dict]:
    """Requests all questions that belong to the specified dimension in the question API
    Calls the `/dimensions/questions/{dimension}` Endpoint

    Parameters
    ----------
    dimension: str
        Represents dimension. Can be `A`, `B`, `C` or any of these three in combination with `1` or `2`

    Returns
    -------
    list[dict]: Array of questions, where the dict represents the question.
        Contains `id` (uuid), `content` (string), `scale` (string), `dimension` (string or null)
    """
    res = requests.get(
        "{0}/v1/dimensions/questions/{1}".format(BASE_URL, dimension))
    if res.status_code != 200:
        raise requests.RequestException(
            f"Request was not fullfilled successfully. Server responded with {res.status_code}")
    return res.json()


def get_scales() -> list[dict]:
    """Requests all scales in the question API
    Calls the `/scales` Endpoint

    Returns
    -------
    list[dict]: Array of objects, where the dict represents the scales.
        Contains `id` (string)
    """
    res = requests.get("{0}/v1/scales".format(BASE_URL))
    if res.status_code != 200:
        raise requests.RequestException(
            f"Request was not fullfilled successfully. Server responded with {res.status_code}")
    return res.json()


def get_answers_for(scale: str) -> list[dict]:
    """ Requests answers for the provided scale in the question API
    Calls the `/answers/{scale}` Endpoint

    Parameters
    ----------
    scale: str
        The scale you want the answers for

    Returns
    -------
    list[dict]: Array of objects, where the dict represents the answers.
        Contains `id` (string), `content` (string), `value` (integer)
    """
    res = requests.get("{0}/v1/answers/{1}".format(BASE_URL, scale))
    if res.status_code != 200:
        raise requests.RequestException(
            f"Request was not fullfilled successfully. Server responded with {res.status_code}")
    return res.json()


def get_scale_for(question: str) -> dict:
    """ Requests scale for the provided question in question API
    Calls the `/scales/{question}` Endpoint

    Parameters
    ----------
    question: str
        The question you want the scale for. Needs to be provided as an UUID

    Returns
    -------
    dict: Object representing the scale
        Contains `id` (string)
    """
    res = requests.get("{0}/v1/scales/{1}".format(BASE_URL, question))
    if res.status_code != 200:
        raise requests.RequestException(
            f"Request was not fullfilled successfully. Server responded with {res.status_code}")
    return res.json()


def get_dimension_description() -> list[dict]:
    """Requests the dimension objects from the question API
    Calls the `/dimensions` Endpoint

    Returns
    -------
    list[dict]: A list with objects, where the dict represents the dimension
        Contains `id` (string), `description` (object) containing `en` (string, english description) and `de` (string, german description)
    """
    res = requests.get("{0}/v1/dimensions".format(BASE_URL))
    if res.status_code != 200:
        raise requests.RequestException(
            f"Request was not fullfilled successfully. Server responded with {res.status_code}")

    return res.json()


def get_dimension_for(question: str) -> dict:
    """Requests the dimension for the provided question from the question API
    Calls the `/questions/{question}/dimension` Endpoint

    Parameters
    ----------
    question: str
        Id of the questions

    Returns
    --------
    dict: Dictionary containing the id of the dimension or None
    """
    res = requests.get(
        "{0}/v1/questions/{1}/dimension".format(BASE_URL, question))
    if res.status_code != 200:
        raise requests.RequestException(
            f"Request was not fullfilled successfully. Server responded with {res.status_code}")

    return res.json()

# Results


def force_courses(courses):
    """Inserts the provided courses with their scores into the score API. Overrides existing data
    Calls the `/scores` Endpoint

    Parameters
    ----------
    courses: list[dict]
        List of objects where each object represents a course with it's scores.
    """
    res = requests.put("{0}/v1/scores".format(BASE_URL), json=courses)

    # If requests are too large, split them
    if res.status_code == 413:
        head = courses[len(courses) // 2:]
        tail = courses[:len(courses) // 2]
        force_courses(head)
        force_courses(tail)
    elif res.status_code != 201:
        raise requests.RequestException(
            f"Request was not fullfilled successfully. Server responded with {res.status_code}")


def get_all_scores() -> list[dict]:
    """Returns all scores that exist in the scores API
    Calls the `/scores` Endpoint

    Returns
    -------
    list[dict]: A list of courses with their score for questions
    """
    res = requests.get("{0}/v1/scores".format(BASE_URL))
    if res.status_code != 200:
        raise requests.RequestException(
            f"Request was not fullfilled successfully. Server responded with {res.status_code}")
    return res.json()


def get_scores_for_dimension(dimension: str) -> list[dict]:
    """Returns all scores that exist in the scores API for the specified dimension
    Calls the `/scores/dimension/{dimension}` Endpoint

    Parameters
    ----------
    dimension: str
        The desired dimension. Can be either `A`, `B`, `C` or any of the three in combination with `1` or `2`

    Returns
    -------
    list[dict]: A list representing the courses with their scores
    """
    res = requests.get(
        "{0}/v1/scores/dimensions/{1}".format(BASE_URL, dimension))
    if res.status_code != 200:
        raise requests.RequestException(
            f"Request was not fullfilled successfully. Server responded with {res.status_code}")
    return res.json()
