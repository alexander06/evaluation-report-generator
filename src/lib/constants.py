# This file keeps commonly used constats throughout the project
import os

RAW_DATA_PATH = os.path.normpath(os.path.join(os.getcwd(), "..", "raw_data"))
CLEAN_DATA_PATH = os.path.normpath(
    os.path.join(os.getcwd(), "..", "clean_data"))
COMPUTER_SCIENCE_VALUES = os.path.join(CLEAN_DATA_PATH, "Informatik")
BIOINFORMATICS_VALUES = os.path.join(CLEAN_DATA_PATH, "Bioinformatik")
MATHEMATICS_VALUES = os.path.join(CLEAN_DATA_PATH, "Mathematik")
OUT_PATH = os.path.normpath(os.path.join(os.getcwd(), '..', "outputs"))

INSTITUTE_MAP = {
    "Bioinformatik": "bio",
    "Mathematik": "math",
    "Informatik": "cs"
}
