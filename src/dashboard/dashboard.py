import streamlit as st
import pandas as pd
import altair as alt

import os
import re
import sys

from functools import cmp_to_key

# Allows access to lib folder
path = os.path.abspath(__file__)
steps = 2
for i in range(steps):
    path = os.path.dirname(path)

sys.path.append(path)

import lib.constants as CONSTANTS  # noqa: E402 (prevents error, since import is supposed to be here)
import lib.api as api  # noqa: E402 (prevents error, since import is supposed to be here)

# Globals
# Allows generation of streamlit interaction elements with unique IDs
global curr_key
curr_key = 0

# Value cache
value_cache = {}

uuid_regex = re.compile(
    '\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b')

colors = {
    "Bioinformatik": "#31CB00",
    "Mathematik": "#008DD5",
    "Informatik": "#A13D63"
}

dimensions = {
    "A": "Vermittlung von Wissen und Unterstützen von Verstehen",
    "B": "Motivieren und lerndienliche Atmosphäre herstellen",
    "C": "Steuerung und Interaktion in der Lerngruppe"
}

map = {
    "cs": "Informatik",
    "bio": "Bioinformatik",
    "math": "Mathematik"
}

# Functions


def api_response_to_dataframe_compatibility(response):
    res = {}
    for course in response:
        course_obj = {
            "Veranstaltungsnummer": course["course_number"],
            "Veranstaltungsname": course["course_name"],
            "Veranstaltungstyp": course["course_type"],
            "Lehrperson": course["lecturer"],
            "Semester": course["semester"],
            "Institut": map[course["institute"]],
            "Antworten": course["answers"],
            "Teilnehmende": course["participants"]
        }

        for question in course["scores"]:
            course_obj[question["question"]] = question["score"]
        res[course["id"]] = course_obj
    return res


def get_interval(percentage):
    initial = [pd.Interval(left=0, right=(1/7), closed="both")]
    if percentage in initial:
        return 1
    left = (1 / 7)
    for i in range(2, 8):
        interval = pd.Interval(left=left, right=left + (1 / 7), closed="right")
        if percentage in interval:
            return i
        left += (1 / 7)


def cache_question(question):
    scale = api.get_scale_for(question)
    dimension = api.get_dimension_for(question)["id"]
    if dimension is None:
        dimension = "no_dim"
    possibilities = api.get_answers_for(scale["id"])
    weights = [answer["value"] for answer in possibilities]
    value_cache[question] = {
        "value": max(weights),
        "dimension": dimension
    }


def convert_scores(scores):
    result = {}
    for course in scores:
        id = course["id"]
        dimensions = []
        result[id] = {
            "Typ": course["course_type"],
            "Institut": map[course["institute"]],
            "Semester": course["semester"],
        }

        maximum = {}

        for score in course["scores"]:
            question = score["question"]

            if question not in value_cache:
                cache_question(question)
            dimension = value_cache[question]["dimension"]

            # Creates initial values
            if dimension not in result[id]:
                result[id][dimension] = 0
                dimensions.append(dimension)
            if dimension not in maximum:
                maximum[dimension] = 0

            result[id][dimension] += score["score"]
            maximum[dimension] += value_cache[question]["value"] * \
                course["answers"]

        # All over score
        for dimension in dimensions:
            if dimension.endswith("1"):
                counterpart = dimension.replace("1", "2")
                if counterpart in dimensions:
                    combined_score = result[id][dimension] + \
                        result[id][counterpart]
                    combined_maximum = maximum[dimension] + \
                        maximum[counterpart]
                    result[id][dimension[0]] = round(
                        ((combined_score + combined_maximum) / (combined_maximum * 2)), 2)
                else:
                    result[id][dimension[0]] = round(
                        ((result[id][dimension] + maximum[dimension]) / (maximum[dimension] * 2)), 2)
            elif dimension.endswith("2") and dimension.replace("2", "1") not in dimensions:
                result[id][dimension[0]] = round(
                    ((result[id][dimension] + maximum[dimension]) / (maximum[dimension] * 2)), 2)

            # Dimension was not evaluated
            if maximum[dimension] == 0 and result[id][dimension] == 0:
                continue
            # Maximum and minimum possible Score based on answers numbers
            # Moves all values into positive integers, simplifying percentage calculation
            result[id][dimension] = round(
                ((result[id][dimension] + maximum[dimension]) / (maximum[dimension] * 2)), 2)

    return result


def percentages_to_interval(scores):
    res = {}
    dimensions = ["A", "A1", "A2", "B", "B1", "B2", "C", "C1", "C2"]
    for course in scores:
        for dimension in dimensions:
            updated = scores[course]
            updated[dimension] = get_interval(updated[dimension])
            res[course] = updated
    return res


def compare_semesters(semester1, semester2):
    if semester1 == semester2:
        return 0

    year1 = int(semester1[4:8])
    year2 = int(semester2[4:8])

    if year1 == year2:
        return 1 if semester1.startswith("Wi") else -1
    elif year1 > year2:
        return 1
    else:
        return -1


def get_min_max_semester(df: pd.DataFrame):
    semester_list = df["Semester"].to_list()
    semester_sorted = sorted(semester_list, key=cmp_to_key(compare_semesters))
    # Oldest, Newsest
    return (semester_sorted[0], semester_sorted[-1])


def generate_semester_interval(left, right):
    interval = []
    start_year = int(left[4:8])
    gen_sose = left.startswith("SoSe")
    year = start_year
    semester = f"SoSe{str(start_year)}" if gen_sose else f"WiSe{str(start_year)}/{str(start_year + 1)[2:4]}"
    while semester != right:
        interval.append(semester)
        gen_sose = not gen_sose
        if gen_sose:
            year += 1
        semester = f"SoSe{str(year)}" if gen_sose else f"WiSe{str(year)}/{str(year + 1)[2:4]}"
    interval.append(semester)

    return interval

# Genrate initial values


# Used by graphs with no names
scores = pd.DataFrame(convert_scores(api.get_all_scores())).T

# For graphs with full names
full_dataset = pd.DataFrame(
    api_response_to_dataframe_compatibility(api.get_all_scores())).T


# Basic Page config
st.set_page_config(
    page_title="Evaluationsbericht",
    layout="wide"
)

with st.sidebar:
    st.header("Inhaltsverzeichnis")
    st.markdown('''
    - [Scatter Plot](#scatter-plot)
    - [Boxplot](#boxplot)
    - [Violinplot](#violinplot)
    - [Linechart](#linechart)
    - [Barchart](#barchart)
    - [Tabelle](#tabelle)
    ''')

st.title("Evaluationsbericht Grafiken")
with st.expander(":orange[ :warning: Achtung!]", expanded=True):
    st.write('''
    Dieses Dashboard ist nur ein triviales Beispiel dafür, wie die Daten die während der Pipeline generiert werden, auf einem Dashboard angezeigt werden können. Dieses Dahboard ist keineswegs ein komplett fertiges Produkt, funktioniert aber minimal genug für die Demonstration.
    ''')
present_mode = st.toggle(
    "Datenschutz Modus (Versteckt Abschnitte mit Personenbezogenen Daten)")

if not present_mode:
    st.sidebar.markdown("- [Ranking](#ranking)")
# If both is chosen, append scale for every semester
semester_select = st.multiselect("Generierung für folgende Semester:", [
                                 "Sommersemester", "Wintersemester"], ["Sommersemester"])


def generate_plot_section__with_subdimensions(title, markdown, chart):
    global curr_key
    st.header(title, divider="gray")
    with st.expander(":information_source: Hinweis", expanded=True):
        st.write(markdown)
    column_multiselect, column_slider = st.columns(2)

    with column_multiselect:
        selected_dimensions = st.multiselect(key=curr_key, label="Dimensionen, die generiert werden sollen", options=[
                                             "A", "B", "C"], default=["A", "B", "C"])
        curr_key += 1
    with column_slider:
        min_semester, max_semester = get_min_max_semester(chart.data)
        interval = generate_semester_interval(min_semester, max_semester)
        interval = [sem for sem in interval if (sem.startswith("So") and "Sommersemester" in semester_select) or (
            sem.startswith("Wi") and "Wintersemester" in semester_select)]

        semester_start, semester_end = st.select_slider(key=curr_key,
                                                        label="Generation für Semester zwischen",
                                                        options=interval,
                                                        value=(
                                                            interval[0], interval[-1])
                                                        )
        curr_key += 1
        semester_range = interval[interval.index(
            semester_start):interval.index(semester_end) + 1]

    chart_columns = st.columns(len(selected_dimensions))

    for (i, dimension) in enumerate(selected_dimensions):
        with chart_columns[i]:
            chart_copy = (chart
                          .transform_filter(alt.FieldOneOfPredicate(field="Semester", oneOf=semester_range))
                          .encode(
                              x=alt.X(f"{dimension}1").scale(
                                  domain=(.20, 1.05)).axis(format="%"),
                              y=alt.Y(f"{dimension}2").scale(
                                  domain=(0, 1.05)).axis(format="%"),
                              tooltip=[alt.Tooltip(f"{dimension}1", format=".2%"), alt.Tooltip(
                                  f"{dimension}2", format=".2%")]
                          )
                          .properties(
                              title=f"Dimension {dimension} - {dimensions[dimension]}"
                          )
                          )
            st.altair_chart(chart_copy, use_container_width=True)


def generate_section_dimensions(title, markdown, chart, type):
    global curr_key
    st.header(title, divider="gray")
    with st.expander(":information_source: Hinweis", expanded=True):
        st.write(markdown)
    column_multiselect, column_slider, column_institute = st.columns(3)

    with column_multiselect:
        selected_dimensions = st.multiselect(key=curr_key, label="Dimensionen, die generiert werden sollen", options=[
                                             "A", "B", "C"], default=["A", "B", "C"])
        curr_key += 1
    with column_slider:
        min_semester, max_semester = get_min_max_semester(chart.data)
        interval = generate_semester_interval(min_semester, max_semester)
        interval = [sem for sem in interval if (sem.startswith("So") and "Sommersemester" in semester_select) or (
            sem.startswith("Wi") and "Wintersemester" in semester_select)]
        semester_start, semester_end = st.select_slider(key=curr_key,
                                                        label="Generation für Semester zwischen",
                                                        options=interval,
                                                        value=(
                                                            interval[0], interval[-1])
                                                        )
        curr_key += 1
        semester_range = interval[interval.index(
            semester_start):interval.index(semester_end) + 1]

    with column_institute:
        selected_institutes = st.multiselect(key=curr_key, label="Generieren für die Institute:", options=[
                                             "Bioinformatik", "Informatik", "Mathematik"], default=["Bioinformatik", "Informatik", "Mathematik"])
        curr_key += 1
    for dimension in selected_dimensions:
        st.subheader("Dimension {0}".format(dimension))
        if type == "box":
            smallest = pd.to_numeric(
                chart.data[dimension]).nsmallest(1).values[0]
            largest = pd.to_numeric(
                chart.data[dimension]).nlargest(1).values[0]
            chart_dim = chart.encode(y=alt.Y(dimension).scale(
                domain=(smallest, largest)).title("Bewertung"))

            columns = st.columns(len(selected_institutes))
            for (i, institute) in enumerate(selected_institutes):
                chart_inst = chart_dim.transform_filter({
                    "and": [
                        alt.FieldOneOfPredicate(
                            field="Semester", oneOf=semester_range),
                        alt.FieldEqualPredicate(
                            field="Institut", equal=institute)
                    ]
                }).encode(color=alt.Color("Institut").scale(domain=[institute], range=[colors[institute]]))
                with columns[i]:
                    st.altair_chart(chart_inst, use_container_width=True)

        elif type == "violin":
            smallest = pd.to_numeric(
                chart.data[dimension]).nsmallest(1).values[0]
            largest = pd.to_numeric(
                chart.data[dimension]).nlargest(1).values[0]
            chart_dim = chart.transform_density(
                dimension,
                as_=[dimension, 'density'],
                groupby=["Semester", "Dimension", "Institut"],
                extent=[0, 7]
            ).mark_area(orient="horizontal").encode(
                x=alt.X("density:Q").stack("center").impute(None).title(
                    None).axis(labels=False, values=[0], grid=False, ticks=True),
                y=alt.Y('{0}:Q'.format(dimension)).scale(
                    domain=(int(smallest - 1), int(largest + 1))).title("Bewertung"),
                tooltip=[alt.Tooltip(
                    "density:Q", title="Dichte", format=".2")],
                color=alt.Color("Semester").legend(None),
                # width 1000 / len ensures that every facet is 200 in width
            ).properties(width=1000 / len(semester_range)).facet(column="Semester", row="Institut")

            chart_inst = chart_dim.transform_filter({
                "and": [
                    alt.FieldOneOfPredicate(
                        field="Semester", oneOf=semester_range),
                    alt.FieldOneOfPredicate(
                        field="Institut", oneOf=selected_institutes)
                ]
            })
            st.altair_chart(chart_inst)


def generate_semester_slider_with_chart(title, markdown, chart):
    global curr_key
    st.header(title, divider="gray")
    with st.expander(":information_source: Hinweis", expanded=True):
        st.write(markdown)

    min_semester, max_semester = get_min_max_semester(chart.data)
    interval = generate_semester_interval(min_semester, max_semester)
    interval = [sem for sem in interval if (sem.startswith("So") and "Sommersemester" in semester_select) or (
        sem.startswith("Wi") and "Wintersemester" in semester_select)]
    semester_start, semester_end = st.select_slider(key=curr_key,
                                                    label="Generation für Semester zwischen",
                                                    options=interval,
                                                    value=(
                                                        interval[0], interval[-1])
                                                    )
    curr_key += 1
    semester_range = interval[interval.index(
        semester_start):interval.index(semester_end) + 1]

    chart = chart.transform_filter(alt.FieldOneOfPredicate(
        field="Semester", oneOf=semester_range))

    st.altair_chart(chart, use_container_width=True)


# Scatterplot
selection = alt.selection_point(fields=["Typ"], bind="legend")
c = (alt.Chart(scores)
     .mark_circle(size=100)
     .encode(
    color=alt.Color("Typ").legend(orient="bottom"),
    opacity=alt.condition(selection, alt.value(1.0), alt.value(0.1))
)
    .interactive()
    .add_params(
    selection
)
)
generate_plot_section__with_subdimensions(
    "Scatter Plot", "Dieser Graph zeigt die Verteilung der Lehrveranstaltungen entlang der entsprechenden Unterdimensionen. Die abgegebenen Punkte werden für jede Frage in der dazugehörigen Dimension addiert und dann mit der maximal erreichbaren Punktzahl verrechnet um den Prozentsatz zu ermitteln. Jeder Punkt repräsentiert eine Lehrveranstaltung.", c)

dimension_explanation_columns = st.columns(3)

dimension_explanation = api.get_dimension_description()

for j in range(2):
    for (i, dimension) in enumerate(["A", "B", "C"]):
        subdimension = "{0}{1}".format(dimension, str(j + 1))
        desc = [dim for dim in dimension_explanation if subdimension in dim][0]
        with dimension_explanation_columns[i]:
            st.markdown('''
              ### Dimension {0}
                {1}
            '''.format(subdimension, desc[subdimension]["de"]))


# Boxplot
box = pd.DataFrame(percentages_to_interval(scores.T.to_dict())).T
c = (alt.Chart(box)
     .mark_boxplot(extent='min-max')
     .encode(
    x="Semester",
    color=alt.Color("Institut").legend(orient="bottom"),
)
    .interactive()
)
generate_section_dimensions("Boxplot", "Dieser Graph zeigt für die Dimensionen des Fragebogens die historische Entwicklung der Fachbereiche. Jeweils eine Spalte bildet einen Fachbereich ab. Die Bewertung entsteht durch aufsummieren aller Studierendenantworten in der jeweiligen Dimension und anschließende Umrechnung in einen Prozentwert mithilfe der maximal erreichbaren Punktzahl. Danach wird der Wert in ein Intervall ähnlich zur Likert Scale von 1-7 eingeordnet. Die Boxen erstecken sich von den unteren 25% bis zu den oberen 75% der Antworten. Alle davor und danach bilden die Schnurrhaare der Box. Der Median ist mit einem Querstrich hervorgehoben.", c, "box")

# Violin Plots
c = (alt.Chart(box).encode(tooltip="Institut").interactive())

generate_section_dimensions("Violinplot", "Dieser Graph zeigt für die Dimensionen des Fragebogens die historische Entwicklung der Fachbereiche. Jeweils eine Spalte bildet einen Fachbereich ab. Die Bewertung entsteht durch aufsummieren aller Studierendenantworten in der jeweiligen Dimension und anschließende Umrechnung in einen Prozentwert mithilfe der maximal erreichbaren Punktzahl. Danach wird der Wert in ein Intervall ähnlich zur Likert Scale von 1-7 eingeordnet. Die Violine erstreckt sich über alle Antworten. Die Breite an den Bewertungen geben an, wie oft diese Bewertung vorkam. Je dicker die Violine an einem Punkt, desto öfter kam die Bewertung vor.", c, "violin")


# Line chart
groups = full_dataset.groupby(by=["Semester", "Institut"])
results = {
    "Semester": [],
    "Institut": [],
    "Beteiligung": []
}
for (header, group) in groups:
    percentage_value = group["Antworten"].sum()

    # Filters out courses without participation data
    basic_value = group[group["Teilnehmende"] > 0]["Teilnehmende"].sum()

    # (empty dataframe due to missing data)
    if basic_value <= 0:
        continue
    results["Semester"].append(header[0])
    results["Institut"].append(header[1])
    results["Beteiligung"].append(round((percentage_value / basic_value), 2))
participation_data = pd.DataFrame(results)
linechart = (
    alt.Chart(participation_data)
    .mark_line(point=True)
    .encode(x="Semester", y=alt.Y("Beteiligung").axis(format="%"), color=alt.Color("Institut").scale(domain=["Bioinformatik", "Mathematik", "Informatik"], range=["#31CB00", "#008DD5", "#A13D63"]))
    .interactive()
)

generate_semester_slider_with_chart("Linechart", "Dieser Graph zeigt den Verlauf der Beteiligung der Studierenden an den Fachbereichen. Die Punkte auf dem Graph sind feste Werte, die nach der Erhebung der Evaluation berechnet werden können, die Verbindungslinien stellen keinen zeitlichen verlauf da, sondern vereinfachen die Visualisierung des An- oder Abstiegs.", linechart)

barchart = (
    alt.Chart(participation_data)
    .mark_bar()
    .encode(x="Semester", y=alt.Y("Beteiligung").axis(format="%"), color=alt.Color("Institut").scale(domain=["Bioinformatik", "Mathematik", "Informatik"], range=["#31CB00", "#008DD5", "#A13D63"]), xOffset="Institut")
    .interactive()
)

generate_semester_slider_with_chart(
    "Barchart", "Dieser Graph zeigt die historische Beteiligung an Lehrevaluationen im direkten Vergleich der Fachbereiche pro Semester an.", barchart)

# Table
st.header("Tabelle", divider="gray")
with st.expander(":information_source: Hinweis", expanded=True):
    st.write('''
    Diese Tabelle zeigt den historischen Verlauf der Beteiligung an Lehrevaluationen über alle Semester (unabhängig vom Filter ganz oben auf der Seite).
    Eine Zelle besteht aus dem aktuellen Beteiligungswert (links) und wie er im historischen Verlauf seit Datenbeginn sich verändert hat (recht).
    Die Richtung des Pfeils indiziert das Wachstum, die Prozentzahl darunter zeigt es genau an.
    Die Farbe des Pfeils zeigt an wie die Entwicklung einzuschätzen ist, nach einem Ampelsystem.
    ''')
template = ""

# This has as precondition that the code is run by the makefile
with open(os.path.join("src", "assets", "table_template.html"), "r") as file:
    template = file.read()


groups = full_dataset.groupby(by=["Semester", "Institut", "Veranstaltungstyp"])

results = {
    "Semester": [],
    "Institut": [],
    "Veranstaltungstyp": [],
    "Beteiligung": []
}
for (header, group) in groups:
    percentage_value = group["Antworten"].sum()
    # Filter out courses with no participation data
    basic_value = group[group["Teilnehmende"] > 0]["Teilnehmende"].sum()

    # (empty dataframe due to missing data)
    if basic_value <= 0:
        continue
    results["Semester"].append(header[0])
    results["Institut"].append(header[1])
    results["Veranstaltungstyp"].append(header[2])
    results["Beteiligung"].append(
        round((percentage_value / basic_value) * 100, 2))
participation_data = pd.DataFrame(results)

for inst in ["Bioinformatik", "Informatik", "Mathematik"]:
    inst_df = participation_data[participation_data["Institut"] == inst]

    semester_list = inst_df["Semester"].to_list()
    oldest_semester = sorted(
        semester_list, key=cmp_to_key(compare_semesters))[0]
    newest_semester = sorted(
        semester_list, key=cmp_to_key(compare_semesters))[-1]

    for category in ["Vorlesungen", "Seminare", "Übungen"]:
        oldest_participation = inst_df[(inst_df["Semester"] == oldest_semester) & (
            inst_df["Veranstaltungstyp"] == category)]["Beteiligung"].values[0]
        newest_participation = inst_df[(inst_df["Semester"] == newest_semester) & (
            inst_df["Veranstaltungstyp"] == category)]["Beteiligung"].values[0]
        difference = round(newest_participation - oldest_participation, 2)

        # -difference ensures correct rotation
        rotation = round(90 * (-difference / 100), 2)

        color = "yellow"

        if difference in pd.Interval(left=5, right=100, closed="both"):
            color = "green"
        elif difference in pd.Interval(left=-100, right=-5, closed="both"):
            color = "red"

        cat_map = {
            "Vorlesungen": "VL",
            "Seminare": "S",
            "Übungen": "U"
        }
        shorthand_start = "%{0}-{1}".format(
            CONSTANTS.INSTITUTE_MAP[inst].upper(), cat_map[category])

        template = template.replace(
            shorthand_start + "%", str(newest_participation) + "%")
        template = template.replace(shorthand_start + "-ROT%", str(rotation))
        template = template.replace(shorthand_start + "-ARROW%", color)
        template = template.replace(
            shorthand_start + "-IND%", str(difference) + "%")

st.write(template, unsafe_allow_html=True)

# Ranking
# Slider for no of ranks
if not present_mode:
    st.header("Ranking", divider="gray")
    with st.expander(":information_source: Hinweis", expanded=True):
        st.markdown("Der Ranking Score wird berechnet indem alle erreichten Punkte für jede Frage summiert werden, diese dann durch die Anzahl an Rückmeldungen geteilt wird und somit die Durchschnittspunkte pro Evaluation erhalten werden")
    # Auto generate
    semester_ranking_select = st.multiselect("Ranking generieren für Semester:", [
                                             "SoSe2019", "SoSe2020", "SoSe2021", "SoSe2022", "SoSe2023"], ["SoSe2023"])
    col1, col2 = st.columns(2)
    col3, col4 = st.columns(2)
    with col1:
        type_select = st.multiselect("Generieren für Kategorien:", [
                                     "Vorlesungen", "Übungen", "Seminare"], ["Vorlesungen"])
    with col2:
        inst_select = st.multiselect("Generieren für die Institute:", [
                                     "Bioinformatik", "Informatik", "Mathematik"], ["Informatik"])
    with col3:
        combine_type = st.toggle("Kategorien kombinieren")
    with col4:
        combine_inst = st.toggle("Institute kombinieren")

    filtered = full_dataset[(full_dataset["Semester"].isin(semester_ranking_select)) & (
        full_dataset["Institut"].isin(inst_select)) & (full_dataset["Veranstaltungstyp"].isin(type_select))]
    cols = [col for col in filtered.columns if uuid_regex.match(
        col) is not None]
    filtered["sum"] = filtered[cols].sum(axis=1)
    dropped = filtered.drop(columns=cols)
    dropped["Punkte"] = dropped["sum"] / dropped["Antworten"]
    dropped["Punkte"] = dropped["Punkte"].apply(lambda x: round(x, 2))
    dropped = dropped.drop(
        columns=["Veranstaltungsnummer", "Antworten", "Teilnehmende", "sum"])
    if not combine_inst:
        for institute in inst_select:
            st.subheader(institute)
            if not combine_type:
                columns = st.columns(len(type_select))
                for i in range(len(type_select)):
                    with columns[i]:
                        st.subheader(type_select[i])
                        frame = dropped[(dropped["Institut"] == institute) & (
                            dropped["Veranstaltungstyp"] == type_select[i])]
                        frame.sort_values(
                            "Punkte", ascending=False, inplace=True)
                        st.dataframe(frame.head(10), hide_index=True,
                                     use_container_width=True)
            else:
                frame = dropped[dropped["Institut"] == institute]
                frame.sort_values("Punkte", ascending=False, inplace=True)
                st.dataframe(frame.head(10), hide_index=True,
                             use_container_width=True)
    else:
        if not combine_type:
            columns = st.columns(len(type_select))
            for i in range(len(type_select)):
                with columns[i]:
                    st.header(type_select[i])
                    frame = dropped[dropped["Veranstaltungstyp"]
                                    == type_select[i]]
                    frame.sort_values("Punkte", ascending=False, inplace=True)
                    st.dataframe(frame.head(10), hide_index=True,
                                 use_container_width=True)
        else:
            frame = dropped.sort_values("Punkte", ascending=False)
            st.dataframe(frame.head(10), hide_index=True,
                         use_container_width=True)
