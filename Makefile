out = clean_data

# Cleaning
clean:
	@$(RM) -rf ./$(out)/*

# Installing
install_javascript:
	cd ./src/db/api && npm i

install_python:
	pip install -r requirements.txt

install: install_javascript install_python

# Doing stuff
streamlit:
	streamlit run src/dashboard/dashboard.py

docker_rebuild: 
	cd ./src/db && docker compose up -d --build

# Linting
jupyter_lint:
	pynblint .

# E501 is line too long
python_lint:
	flake8 --extend-ignore=E501 ./src

javascript_lint:
	cd ./src/db/api && npx prettier -c ./

lint: jupyter_lint python_lint javascript_lint

# Code
jupyter:
	jupyter lab ./src

run_pipeline_full:
	make install && make docker_rebuild && jupyter execute ./src/cleanup.ipynb ./src/old-report.ipynb ./src/pipeline.ipynb && make streamlit

# Expects that install and docker rebuild has already been called
run_pipeline:
	jupyter execute ./src/cleanup.ipynb ./src/old-report.ipynb ./src/pipeline.ipynb && make streamlit